
import javax.swing.JButton;
//javax.swing -> elementos de la interfaz gráfica
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

// java.awt -> características y esquemas para la interfaz gráfica
import java.awt.*;
//java.awt.event -> contiene los eventos de la interfaz gráfica
import java.awt.event.*;

public class CalculadoraUI extends JFrame {
    /*************
     * Atributos
     ************/
    private JTextField txtResultado;
    private JButton btn_1;
    private JButton btn_2;
    private JButton btn_3;
    private JButton btn_4;
    private JButton btn_sumar;
    private JButton btn_igual;

    public CalculadoraUI(){
        //Configuración de la ventana
        this.setTitle("Mi primera calculadora");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(0,0, 420, 300);

        //Añadir esquema a la ventana borderlayout sin espacios
        BorderLayout esquema = new BorderLayout();
        //Obtener el contenedor por defecto de la ventana y añadirle el esquema
        this.getContentPane().setLayout(esquema);

        /********************************
         * PARTE SUPERIOR DE LA VENTANA
         ********************************/
        this.txtResultado = new JTextField();
        this.add(this.txtResultado, BorderLayout.NORTH);


         /********************************
         * PARTE CENTRAL DE LA VENTANA
         ********************************/
        //Crear un container con gridlayout de 2 x 2
        //this.add(container, BorderLayout.CENTER);


         /********************************
         * PARTE LATERAL DERECHA DE LA VENTANA
         ********************************/
         //this.add(elemento, BorderLayout.EAST);

         /********************************
         * PARTE INFERIOR DE LA VENTANA
         ********************************/
         //this.add(elemento, BorderLayout.SOUTH);
    }

}